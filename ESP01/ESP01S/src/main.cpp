#include <Arduino.h>

#include <DHTesp.h>
#include <ESP8266WiFi.h>
#include <AsyncMqttClient.h>

#include "config.h"

WiFiClient espClient;
AsyncMqttClient mqttClient;

DHTesp dht;

const char* device = "esp01_a1";
const char* TempTopic = "env/esp01_a1/1";
const char* HumTopic = "env/esp01_a1/2";
volatile bool isWokenUp = false;
int sleepCount = 0;

void initWifi();
void lightSleep();
void wakeupCallback();

void setup() {
  Serial.begin(74880);

  dht.setup(2, DHTesp::DHT22);

  mqttClient.setClientId(device);
  mqttClient.setServer(mqtt_server, 1883);

  initWifi();
}

void loop() {

  if (sleepCount == 0) {
    initWifi();

    float temperature = dht.getTemperature();
    if (!isnan(temperature)) {
      mqttClient.publish(TempTopic, 0, false, String(temperature).c_str());
    }

    delay(1000);

    float humidity = dht.getHumidity();
    if (!isnan(humidity)) {
      mqttClient.publish(HumTopic, 0, false, String(humidity).c_str());
    }
  }

  lightSleep();

  if (sleepCount == sleepCountMax) {
    sleepCount = 0;
  } else {
    sleepCount++;
  }
}


void initWifi(){
  WiFi.mode(WIFI_STA);
  WiFi.begin(wifi_ssid, wifi_password);
    while ((WiFi.status() != WL_CONNECTED)) {
        delay(500);
        Serial.print(".");
    }
  Serial.println("");
  Serial.print("WiFi connected, IP address: "); Serial.println(WiFi.localIP());

  mqttClient.connect();
  while (!mqttClient.connected()) {
    delay(1000);
    Serial.println("Connecting to MQTT...");
  }
}

void lightSleep() {
    Serial.println("Sleeping");
    Serial.flush();
    mqttClient.disconnect();
    WiFi.mode(WIFI_OFF);
    extern os_timer_t *timer_list;
    timer_list = nullptr;
    wifi_fpm_set_sleep_type(LIGHT_SLEEP_T);
    wifi_fpm_set_wakeup_cb(wakeupCallback);
    wifi_fpm_open();
    wifi_fpm_do_sleep(sleepTimeSeconds * 1000 * 1000);
    while (!isWokenUp) { // Wait for the device to wake up
        delay(1000);
    }
    isWokenUp = false;
}

void wakeupCallback() {
    Serial.println("Wakeup");
    Serial.flush();
    isWokenUp = true;
}