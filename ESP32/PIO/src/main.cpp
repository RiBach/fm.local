#include <Arduino.h>
#include <Wire.h>

#include "defines.h"
#include "I2Cdev.h"
#include "MPU6050.h"

#include <WiFi.h>
extern "C" {
	#include "freertos/FreeRTOS.h"
	#include "freertos/timers.h"
}
#include <AsyncMqttClient.h>

AsyncMqttClient mqttClient;

const char* MQTT_CLIENT_ID = "ESP32-Client-0.1";
const char* AccTopic = "esp/ESP32-Client-0.1/a";
const char* HallTopic = "esp/ESP32-Client-0.1/hall";

void connectToMqtt() {
  Serial.println("Connecting to MQTT...");
  mqttClient.connect();
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason) {
  Serial.println("Disconnected from MQTT.");

  if (WiFi.status() == WL_CONNECTED) {
    connectToMqtt();
  }
  else ESP.restart();
}

long lastAccMsg = 0;
MPU6050 mpu;
int16_t ax, ay, az;

void setup(){
  Serial.begin(115200);

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  int attempts = 0;
  while (WiFi.status() != WL_CONNECTED) {
    Serial.println("Connecting to WiFi...");
    delay(2000);
    Serial.println(".");
    attempts++;
    if (attempts > 3){
      Serial.println("Failed to connect to WiFi");
      ESP.restart();
    }
  }

  mqttClient.setClientId(MQTT_CLIENT_ID);
  mqttClient.onDisconnect(onMqttDisconnect);
  mqttClient.setServer(MQTT_HOST, MQTT_PORT);
  mqttClient.connect();

  Wire.begin();
  mpu.initialize();
}

void loop(){

  mpu.getAcceleration(&ax, &ay, &az);

  long now = millis();
  if (now - lastAccMsg > 4) {
    lastAccMsg = now;

    char accString[22] = "";
    char temp[7] = "";

    itoa(ax, temp, 10);
    strcat(accString, temp);
    strcat(accString, " ");

    itoa(ay, temp, 10);
    strcat(accString, temp);
    strcat(accString, " ");

    itoa(az, temp, 10);
    strcat(accString, temp);

    mqttClient.publish(AccTopic, 0, false, accString);
  }
}