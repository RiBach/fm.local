﻿using MQTTnet;
using MQTTnet.Server;

var mqttFactory = new MqttFactory();

var mqttServerOptions = new MqttServerOptionsBuilder()
    .WithDefaultEndpoint()
    .Build();

using var mqttServer = mqttFactory.CreateMqttServer(mqttServerOptions);

// mqttServer.ValidatingConnectionAsync += e =>
// {
//     if (e.UserName != "user" || e.Password != "password")
//     {
//         e.ReasonCode = MqttConnectReasonCode.BadUserNameOrPassword;
//     }
//
//     return Task.CompletedTask;
// };

await mqttServer.StartAsync();

Thread.Sleep(Timeout.Infinite);