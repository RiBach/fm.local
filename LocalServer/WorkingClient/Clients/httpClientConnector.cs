using System.Net.Http.Json;

namespace WorkingClient.Clients;

public class HttpClientConnector
{
    private readonly HttpClient _httpClient = new()
    {
        // Ignores DNS
        BaseAddress = new Uri("http://host.docker.internal:5072/")
    };

    public async Task Send(JsonContent content, string url)
    {
        await _httpClient.PostAsync(url, content);
    }
}