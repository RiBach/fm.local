﻿using System.Net.Http.Json;
using System.Text;
using MQTTnet;
using MQTTnet.Client;
using WorkingClient.Clients;

// SHELLY CHANGE TIMER (in seconds)
// http://192.168.0.80/settings?mqtt_update_period=1

var httpClient = new HttpClientConnector();

var mqttFactory = new MqttFactory();

using var mqttClient = mqttFactory.CreateMqttClient();
var mqttClientOptions = new MqttClientOptionsBuilder()
    .WithTcpServer("host.docker.internal", 1883)
    .WithClientId("WorkingClient")
    // .WithCredentials("user", "password")
    .Build();

var mqttSubscribeOptions = mqttFactory.CreateSubscribeOptionsBuilder()
    .WithTopicFilter(f => f.WithTopic("esp/#"))
    .WithTopicFilter(f => f.WithTopic("shellies/+/relay/0/power"))
    .WithTopicFilter(f => f.WithTopic("shellies/+/relay/0/energy"))
    .WithTopicFilter(f => f.WithTopic("env/#"))
    .Build();


string topicHeader, clientId, dataTopic;
JsonContent content;
var energyLast = 0;
var batteryLast = 0;

mqttClient.ApplicationMessageReceivedAsync += async e =>
{
    //TODO: think about multiple devices on a network, possibly another topic header as a Cassandra keyspace (LOW PRIORITY)
    var data = Encoding.UTF8.GetString(e.ApplicationMessage.PayloadSegment);

    var timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

    var topicArray = e.ApplicationMessage.Topic.Split('/');
    topicHeader = topicArray[0];
    clientId = topicArray[1];

    switch (topicHeader)
    {
        case "esp":
            dataTopic = topicArray[2];

            switch (dataTopic)
            {
                case "a":
                    var accData = data.Split(' ');

                    int.TryParse(accData[0], out var accX);
                    int.TryParse(accData[1], out var accY);
                    int.TryParse(accData[2], out var accZ);

                    content = JsonContent.Create(new
                    {
                        Timestamp = timestamp,
                        X = accX,
                        Y = accY,
                        Z = accZ
                    });
                    await httpClient.Send(content, "acceleration");

                    break;

                case "hall":
                    content = JsonContent.Create(new
                    {
                        Timestamp = timestamp,
                        HallEffect = data
                    });
                    await httpClient.Send(content, "mag");

                    break;
            }
            break;

        // topic = shellies/plug/relay/0/(power):(energy)
        case "shellies":
            dataTopic = topicArray[^1];

            switch (dataTopic)
            {
                // Watts
                case "power":
                    content = JsonContent.Create(new
                    {
                        Timestamp = timestamp,
                        Power = data
                    });

                    await httpClient.Send(content, "power");
                    break;

                // total amount of energy consumed in Watt-minute
                case "energy":
                    var energyNow = int.Parse(data);

                    if (energyLast != energyNow)
                    {
                        energyLast = energyNow;
                        content = JsonContent.Create(new
                        {
                            Timestamp = timestamp,
                            Energy = data
                        });
                        await httpClient.Send(content, "energy");
                    }
                    break;
            }
            break;

        // topic = env/{id}/1
        // 1 : temperature (58 == 5.8°C)
        // 2 : humidity (339 == 33.9%)
        // 4 : battery (100 == 100%)
        case "env":
            dataTopic = topicArray[2];

            switch (dataTopic)
            {
                case "1":
                    Console.WriteLine($"{DateTime.Now.TimeOfDay:hh\\:mm\\:ss} : {clientId} - {data}");
                    content = JsonContent.Create(new
                    {
                        SensorId = clientId,
                        Timestamp = timestamp,
                        Temperature = data
                    });
                    await httpClient.Send(content, "temperature");
                    break;

                case "2":
                    Console.WriteLine($"{DateTime.Now.TimeOfDay:hh\\:mm\\:ss} : {clientId} - {data}");
                    content = JsonContent.Create(new
                    {
                        SensorId = clientId,
                        Timestamp = timestamp,
                        Humidity = data
                    });
                    await httpClient.Send(content, "humidity");
                    break;

                case "4":
                    var batteryNow = int.Parse(data);

                    if (batteryLast != batteryNow)
                    {
                        batteryLast = batteryNow;
                        content = JsonContent.Create(new
                        {
                            SensorId = clientId,
                            Timestamp = timestamp,
                            Battery = data
                        });
                        await httpClient.Send(content, "battery");
                    }
                    break;
            }
            break;
    }
};

mqttClient.ConnectedAsync += e =>
{
    Console.WriteLine($"{DateTime.Now.TimeOfDay:hh\\:mm\\:ss} : Connected to broker");
    return Task.CompletedTask;
};

mqttClient.DisconnectedAsync += async e =>
{
    try
    {
        await Task.Delay(TimeSpan.FromSeconds(5));
        await mqttClient.ReconnectAsync();
        await mqttClient.SubscribeAsync(mqttSubscribeOptions, CancellationToken.None);
    }
    catch (Exception)
    {
        Console.WriteLine($@"{DateTime.Now.TimeOfDay:hh\:mm\:ss} : Reconnection to broker failed");
    }
};

while (mqttClient.IsConnected is not true)
{
    try
    {
        Console.WriteLine($@"{DateTime.Now.TimeOfDay:hh\:mm\:ss} : Connecting to broker...");
        await mqttClient.ConnectAsync(mqttClientOptions, CancellationToken.None);
        await mqttClient.SubscribeAsync(mqttSubscribeOptions, CancellationToken.None);
    }
    catch (Exception)
    {
        await Task.Delay(TimeSpan.FromSeconds(5));
    }
}

await mqttClient.SubscribeAsync(mqttSubscribeOptions, CancellationToken.None);

Thread.Sleep(Timeout.Infinite);