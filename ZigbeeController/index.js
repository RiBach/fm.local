const TuyAPI = require("tuyapi");
const mqtt = require("mqtt");

const brokerUrl = "mqtt://host.docker.internal";
const clientId = "EnvironmentClient";

const options = {
  clientId: clientId,
  reconnectPeriod: 5000,
};

const client = mqtt.connect(brokerUrl, options);

// Handle error event
client.on("error", (error) => {
  // console.error("Error:", error);
});

client.on("connected", () => {
  console.log("Connected to MQTT broker");
});

//TODO - Add reconnection
const device = new TuyAPI({
  id: "bf654aa071a4db112bvovn",
  key: "yTGNin[7TFz0n1?t",
});

connect();

device.on("connected", () => {
  console.log("Connected to device!");
});

device.on("disconnected", () => {
  console.log("Disconnected from device.");
  reconnect();
});

device.on("dp-refresh", (data) => {
  console.log(data);
  publish(data);
});

device.on("data", (data) => {
  console.log(data); //{ dps: { '1': 66 }, cid: 'a4c1384bba230fc2', t: 1712859818 }
  publish(data);
});

// dps: 1 : temperature (58 == 5.8°C)
// dps: 2 : humidity (339 == 33.9%)
// dps: 3 : ?
// dps: 4 : batterty (100 == 100%)

function publish(data) {
  const { dps, cid } = data;
  if (dps) {
    const id = Object.keys(dps);
    const value = Object.values(dps);

    if (cid) {
      client.publish(
        `env/${"th_" + cid.toString()}/${id[0].toString()}`,
        value[0].toString()
      );
    }
  }
}

function connect() {
  device
    .find()
    .then(() => {
      device.connect().catch((error) => {
        console.error("Failed to reconnect:", error);
        reconnect();
      });
    })
    .catch((error) => {
      console.error("Failed to find device:", error);
      reconnect();
    });
}

function reconnect() {
  setTimeout(() => {
    console.log("Attempting to reconnect...");
    connect();
  }, 5000);
}
